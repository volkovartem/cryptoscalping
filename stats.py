import os


def to_excel_log(data):
    general_log_path = 'balances_log.csv'
    # general_log_path = '/home/ubuntu/cryptoscalping/balances_log.csv'

    if os.path.exists(general_log_path) is False:
        header = ''
        for column in list(data.keys()):
            header += column + ';'
        f = open(general_log_path, "a")
        f.write(header + '\n')
        f.close()

    line = ''
    for column in list(data.keys()):
        line += data[column] + ';'
    if line != '':
        f = open(general_log_path, "a")
        f.write(line + '\n')
        f.close()


def update_balances(bclient):
    balances = bclient.get_account()['balances']
    data = dict({balance['asset']: balance['free'] for balance in balances})
    to_excel_log(data)
