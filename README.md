# Cryptoarbiter test. Deploy on server Ubuntu 16

## INSTALL PYTHON & PIP

`sudo add-apt-repository ppa:jonathonf/python-3.6`  

> if it gives  ***add-apt-repository: command not found***   than use: `sudo apt-get install software-properties-common`

**Put each command separatly, one by one**
```
sudo apt update
sudo apt install python3.6
sudo apt install python3.6-dev
sudo apt install python3.6-venv
wget https://bootstrap.pypa.io/get-pip.py
sudo python3.6 get-pip.py
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python3
sudo ln -s /usr/local/bin/pip /usr/local/bin/pip3
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python
```



## Install cryptoscalping and memcached

```
apt-get install git-core
git clone https://volkovartem@bitbucket.org/volkovartem/cryptoscalping.git
cd ~/cryptoscalping; . decommenter.sh
apt install memcached
```


## Creating virtualenv using Python 3.6

```
sudo pip install virtualenv
virtualenv -p /usr/bin/python3.6 ~/cryptoscalping/venv
cd ~/cryptoscalping; . venv/bin/activate
pip install -r requirements.txt
deactivate
```


## Install & config supervisor


```
sudo apt-get install supervisor
sudo cp cryptoscalping.conf /etc/supervisor/conf.d/cryptoscalping.conf
sudo mkdir /var/log/cryptoscalping
sudo supervisorctl reread
sudo supervisorctl reload
```



## Start APP

You can put all these commands at once

```
sudo supervisorctl start wsBinance
sudo sleep 5
sudo supervisorctl start pusher
sudo sleep 5
sudo supervisorctl start monitor
sudo sleep 1
sudo echo LAUNCHED
sudo supervisorctl status
```


## Usefull commands

```
sudo supervisorctl tail -5000 wsBinance stderr
sudo supervisorctl status
cd ~/cryptoarbiter; . venv/bin/activate
sudo nano /var/log/cryptoscalping/wsBinance_err.log
```







