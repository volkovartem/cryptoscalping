import json
import threading
import time

import models
from trade import place_pending_order
from utils import get_candles, get_current_candle, get_ma_value, is_allowed, get_pip, \
    get_current_price, get_preferences, get_precision, to_general_log
from pymemcache.client.base import Client
from binance.client import Client as bClient


def check_the_last_order(symbol, side, price, client):
    cache = client.get(symbol + '_last_order')
    if cache is not None:
        last_order = json.loads(cache.decode())
        last_order_side = last_order[0]
        last_order_price = float(last_order[1])
        if side == 'BUY':
            if last_order_side == 'SELL' and last_order_price < price:
                return False
        if side == 'SELL':
            if last_order_side == 'BUY' and last_order_price > price:
                return False
    return True


def new_signal_report(symbol, risk, entrance_point, stop_loss, take_profits, client):
    text = 'NEW SIGNAL > risk: {}, entrance: {}, SL: {}, TP1: {}, TP2: {}'\
        .format(risk, entrance_point, stop_loss, take_profits[0]['price'], take_profits[1]['price'])
    to_general_log(symbol, text, client)


def last_bars_extremum(symbol, number, side, client):
    last_candles = get_candles(symbol, '5M', client)[499 - number:499]

    if side == 'buy':
        # return max high
        return max(list(float(x[2]) for x in last_candles))

    if side == 'sell':
        # return min low
        return min(list(float(x[3]) for x in last_candles))


def check_anchor_chart(symbol, side, client):
    # return True
    ma8 = get_ma_value(symbol, '1H', '8', client)
    ma21 = get_ma_value(symbol, '1H', '21', client)

    current_candle_high = float(get_current_candle(symbol, '1H', client)[2])
    current_candle_low = float(get_current_candle(symbol, '1H', client)[3])

    if side == 'buy':
        if ma21 < ma8 < current_candle_low:
            return True

    if side == 'sell':
        if ma21 > ma8 > current_candle_high:
            return True

    return False


def launch(symbol, preferences, client):
    minimal_quantity = preferences['minimal_quantity']
    volume24_threshold_bnb = float(preferences['volume24_threshold_BNB'])
    b_client = bClient(preferences['api_key'], preferences['secret_key'])
    trade_fee = float(preferences['trade_fee']) / 100
    preferred_wallet = preferences['preferred_wallet']
    ignore_stop_loss_buy = preferences['ignore_stop_loss_buy']
    precision = get_precision(symbol, client)
    pip = get_pip(symbol, client)
    risk_threshold = 2.1 * int(get_current_price(symbol, 'bid', client) * trade_fee * 2 / pip)

    to_general_log(symbol, 'Start monitoring', client)

    while is_allowed(symbol, volume24_threshold_bnb, client):
        ma8 = get_ma_value(symbol, '5M', '8', client)
        ma21 = get_ma_value(symbol, '5M', '21', client)

        current_candle_open = float(get_current_candle(symbol, '5M', client)[1])
        current_candle_close = float(get_current_candle(symbol, '5M', client)[4])

        # looking for BUY signal
        if ma21 < ma8 < min(current_candle_open, current_candle_close) and check_anchor_chart(symbol, 'buy', client):

            # waiting trigger bar for BUY
            while True:
                current_candle = get_current_candle(symbol, '5M', client)
                current_candle_low = float(current_candle[3])
                current_candle_close = float(current_candle[4])
                ma8 = get_ma_value(symbol, '5M', '8', client)

                if current_candle_close < ma8:
                    # to_log(symbol, 'Trigger bar for BUY')

                    # Stop loss = 3 pips below trigger bar low
                    stop_loss = current_candle_low - (30 * pip)
                    # Buy stop = 3 pips above high last 5 bars
                    entrance_point = last_bars_extremum(symbol, 5, 'buy', client) + (30 * pip)
                    # initial risk R
                    risk = round(entrance_point - stop_loss, 7)
                    # TP1 = risk X 1
                    target1 = round(entrance_point + risk, 7)
                    # TP2 = risk X 2
                    target2 = round(entrance_point + (risk * 2), 7)

                    if risk < risk_threshold * pip:
                        # to_log(symbol, 'Risk is too low. {} < {}'.format(risk, risk_threshold))
                        break

                    if check_the_last_order(symbol, 'BUY', entrance_point, client) is False:
                        break

                    take_profits = [{'target': 1, 'price': target1, 'amount_percent': 50},
                                    {'target': 2, 'price': target2, 'amount_percent': 50}]

                    # trigger_bar_report(symbol, stop_loss, entrance_point, risk, take_profits)
                    new_signal_report(symbol, risk, entrance_point, stop_loss, take_profits, client)

                    place_pending_order(symbol, 'buy', entrance_point, minimal_quantity, stop_loss, take_profits,
                                        preferred_wallet, precision, ignore_stop_loss_buy, b_client, client)
                    break

        # looking for SELL signal
        if ma21 > ma8 > max(current_candle_open, current_candle_close) and check_anchor_chart(symbol, 'sell', client):

            # waiting trigger bar for SELL
            while True:
                current_candle = get_current_candle(symbol, '5M', client)
                current_candle_open = float(current_candle[1])
                current_candle_high = float(current_candle[2])
                ma8 = get_ma_value(symbol, '5M', '8', client)

                if current_candle_open > ma8:
                    # to_log(symbol, 'Trigger bar for SELL')

                    # Stop loss = 3 pips above trigger bar high
                    stop_loss = current_candle_high + (30 * pip)
                    # Sell stop = 3 pips below low last 5 bars
                    entrance_point = last_bars_extremum(symbol, 5, 'sell', client) - (30 * pip)
                    # initial risk R
                    risk = round(stop_loss - entrance_point, 7)
                    # TP1 = risk X 1
                    target1 = round(entrance_point - risk, 7)
                    # TP2 = risk X 2
                    target2 = round(entrance_point - (risk * 2), 7)

                    if risk < risk_threshold * pip:
                        # to_log(symbol, 'Risk is too low. {} < {}'.format(risk, risk_threshold))
                        break

                    if check_the_last_order(symbol, 'BUY', entrance_point, client) is False:
                        break

                    take_profits = [{'target': 1, 'price': target1, 'amount_percent': 50},
                                    {'target': 2, 'price': target2, 'amount_percent': 50}]

                    # trigger_bar_report(symbol, stop_loss, entrance_point, risk, take_profits)
                    new_signal_report(symbol, risk, entrance_point, stop_loss, take_profits, client)

                    place_pending_order(symbol, 'sell', entrance_point, minimal_quantity, stop_loss, take_profits,
                                        preferred_wallet, precision, ignore_stop_loss_buy, b_client, client)
                    break

        time.sleep(1)


def launcher():
    preferences = get_preferences()

    threads = []

    for symbol in preferences['symbols']:
        th = threading.Thread(target=launch,
                              kwargs={"symbol": symbol,
                                      "preferences": preferences,
                                      "client": Client(('127.0.0.1', 11211))})
        th.start()
        threads.append(th)


if __name__ == '__main__':
    try:
        models.initialize()
        launcher()
    except KeyboardInterrupt:
        exit()

# print(check_anchor_chart('BNBBTC', 'buy'))
# print(check_anchor_chart('BNBBTC', 'sell'))

# print(last_bars_extremum('BNBBTC', 5, 'buy'))
# print(last_bars_extremum('BNBBTC', 5, 'sell'))
