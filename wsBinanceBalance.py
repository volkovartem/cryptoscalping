# import sys
# sys.path.append('/root/cryptoarbiter')
import json

from binance.websockets import BinanceSocketManager
from binance.client import Client

from pymemcache.client.base import Client as mClient

from utils import get_preferences

name = 'Binance'
m_client = mClient(('127.0.0.1', 11211))


def process_message(message):
    # print(message)
    if message['e'] == 'outboundAccountInfo':
        balances = message['B']
        xd = []
        for balance in balances:
            xd.append({'asset': balance['a'], 'free': balance['f']})
            m_client.set('BinanceBalance', json.dumps(xd))
        print('WS: balances updated', len(xd))
        print(xd)


def launch():
    api_key = get_preferences()['api_key']
    secret_key = get_preferences()['secret_key']

    client = Client(api_key, secret_key)

    from binance.client import Client as bClient
    preferences = get_preferences()
    b_client = bClient(preferences['api_key'], preferences['secret_key'])
    balances = b_client.get_account()['balances']

    if balances is not None:
        m_client.set(name + 'Balance', json.dumps(balances))
        print('balances', len(balances), balances)
    else:
        print('websocket bibox error balances')
        return

    m_client.set('BinanceBalance', json.dumps(balances))
    print('REST: balances updated', len(balances))
    bm = BinanceSocketManager(client)
    conn_key = bm.start_user_socket(process_message)
    print(conn_key)
    bm.start()


launch()
