import datetime
import smtplib as smtp
import statistics

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import time

from database import get_trades, convert
from utils import get_preferences, split_symbol
from pymemcache.client.base import Client


def send_email(trades):
    email = get_preferences()['email_login_yandex']
    password = get_preferences()['email_password_yandex']
    dest_email = get_preferences()['destination_address']

    message = MIMEMultipart("alternative")
    message["Subject"] = "Report"
    message["From"] = email
    message["To"] = dest_email

    # Create the plain-text and HTML version of your message
    text = """\
    Hi, over the past 12 hours, the bot made the following trades"""

    component = """"""
    for trade in trades:
        block = '''
        <tr>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
        </tr>'''.format(trade['asset'],
                        trade['b_price'],
                        trade['s_price'],
                        'Yes' if trade['sl'] else 'No',
                        trade['profit'])
        component += block

    html = """\
    <html>
      <body>
        <p>Hi, over the past 12 hours, the bot made the following trades<p>
        <div>
          <tr>
            <td><h3>Asset</h3></td>
            <td><h3>* Buying Price *</h3></td>
            <td><h3>Selling Price</h3></td>
            <td><h3>* Stop Loss *</h3></td>
            <td><h3>Profit</h3></td>
          </tr>
          {}
        </div>
      </body>
    </html>
    """.format(component)

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    server = smtp.SMTP_SSL('smtp.yandex.com')
    server.set_debuglevel(1)
    server.ehlo(email)
    server.login(email, password)
    server.auth_plain()
    server.sendmail(email, dest_email, message.as_string())
    server.quit()


def make_stats(hours, client):
    preferred_wallet = get_preferences()['preferred_wallet']
    timestamp = int(time.time()) - hours * 3600
    trades = get_trades()
    signals = list(dict.fromkeys(list(x['signal_id'] for x in trades)))
    signals_list = []
    for signal in signals:
        s_time = 9999999999
        s_symbol = ''
        buy_prices = []
        sell_prices = []
        is_stop_loss = False
        for trade in trades:
            if trade['signal_id'] == signal:
                s_symbol = trade['symbol']
                if trade['date_create'] < s_time:
                    s_time = trade['date_create']
                if trade['side'] == 'BUY':
                    buy_prices.append(trade['price'])
                if trade['side'] == 'SELL':
                    sell_prices.append(trade['price'])
                if trade['type'] == 'SL':
                    is_stop_loss = True

        if s_time >= timestamp:
            s_buy_price = statistics.mean(buy_prices) if buy_prices != [] else 0
            s_sell_price = statistics.mean(sell_prices) if sell_prices != [] else 0
            s_profit = "{0:.6f}".format(convert(split_symbol(s_symbol, client)['base'], s_sell_price - s_buy_price,
                                                preferred_wallet, client))
            signals_list.append({'asset': s_symbol,
                                 'b_price': s_buy_price,
                                 's_price': s_sell_price,
                                 'sl': is_stop_loss,
                                 'profit': s_profit})
    return signals_list


def launch():
    client = Client(('127.0.0.1', 11211))
    while True:
        time.sleep(1)
        schedule = get_preferences()['notifications_schedule_utc']
        time_now = datetime.datetime.utcnow().strftime('%H:%M')
        time_range = get_preferences()['time_range']
        if time_now in schedule:
            stats = make_stats(time_range, client)
            if stats:
                send_email(stats)


if __name__ == '__main__':
    try:
        launch()
    except KeyboardInterrupt:
        exit()
