import json

import utils

ff = open("preferences.txt", "r")
preferences = json.loads(ff.read())
symbol = preferences['symbol']
quantity = preferences['quantity']
current_price = utils.get_current_price(symbol, 'bid')

side = 'buy'
price = current_price
stoploss = current_price - 0.00001
takeprofits = [{'target': 1, 'price': current_price + 0.000005, 'amount_percent': 50},
               {'target': 2, 'price': current_price + 0.00001, 'amount_percent': 100}]

print('stoploss', stoploss)
print('takeprofits', takeprofits)

# utils.place_pending_order(symbol, side, price, quantity, stoploss, takeprofits)
# utils.place_pending_order(symbol, side, price, quantity, stoploss, takeprofits)
