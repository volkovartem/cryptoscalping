import json
import time
import traceback

from binance.client import Client as bClient
from pymemcache.client.base import Client

# settings
from utils import get_preferences

to_volume = int(get_preferences()['frequency_volume_update_sec'])
t_volume = time.time() - to_volume
to_mi = 300
t_mi = time.time() - to_mi


def push_market_info(client, b_client):
    global t_mi
    if time.time() - t_mi > to_mi:
        exchange_info = b_client.get_exchange_info()
        if exchange_info is not None:
            client.set('exchange_info', json.dumps(exchange_info))
            print('exchange_info', len(exchange_info), exchange_info)
        t_mi = time.time()
        time.sleep(0.5)


def push_volume(client, b_client):
    global t_volume
    if time.time() - t_volume > to_volume:
        tickers = b_client.get_ticker()
        if tickers is not None:
            client.set('tickers', json.dumps(tickers))
            print('tickers', len(tickers), tickers)
        t_volume = time.time()
        time.sleep(0.5)


def launch():
    b_client = bClient(get_preferences()['api_key'], get_preferences()['secret_key'])
    client = Client(('127.0.0.1', 11211))

    while True:
        try:
            push_volume(client, b_client)
            push_market_info(client, b_client)
            time.sleep(0.01)
        except KeyboardInterrupt:
            break
        except:
            print(traceback.format_exc())
            time.sleep(5)


if __name__ == '__main__':
    try:
        launch()
    except KeyboardInterrupt:
        exit()

