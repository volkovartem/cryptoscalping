import json

from pymemcache.client.base import Client

log_file_path = 'log/general.log'
# log_file_path = '/home/ubuntu/cryptoscalping/log/general.log'


def generate():
    client = Client(('127.0.0.1', 11211))
    log = client.get('general_log')
    if log is not None:
        log = json.loads(log)
        f = open(log_file_path, "w")
        for text in log:
            f.write(text + '\n')
            print(text)
        f.close()
        print('Done\nCheck {}'.format(log_file_path))
    else:
        print('Log is empty')


if __name__ == '__main__':
    try:
        generate()
    except KeyboardInterrupt:
        exit()
