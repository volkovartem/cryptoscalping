import json
import time
import traceback

from utils import get_current_candle, get_ma_value, get_current_price, get_quantity, save_trade, get_qty, to_general_log

signal_number = 0


def is_take_profit(symbol, side, take_profits, target, client):
    if side == 'buy':
        return True if get_current_price(symbol, 'bid', client) >= take_profits[target]['price'] else False
    else:
        return True if get_current_price(symbol, 'ask', client) <= take_profits[target]['price'] else False


def is_stop_loss(symbol, side, stop_loss, client):
    if side == 'buy':
        return True if get_current_price(symbol, 'bid', client) <= stop_loss else False
    else:
        return True if get_current_price(symbol, 'ask', client) >= stop_loss else False


def is_entrance(symbol, side, price, client):
    if side == 'buy':
        return True if get_current_price(symbol, 'ask', client) >= price else False
    else:
        return True if get_current_price(symbol, 'bid', client) <= price else False


def is_cancel(symbol, side, client):
    current_candle_open = float(get_current_candle(symbol, '5M', client)[1])
    current_candle_close = float(get_current_candle(symbol, '5M', client)[4])

    if side == 'buy':
        if min(current_candle_open, current_candle_close) < get_ma_value(symbol, '5M', '21', client):
            return True
    else:
        if max(current_candle_open, current_candle_close) > get_ma_value(symbol, '5M', '21', client):
            return True
    return False


def execute_stop_loss(symbol, side, ignore_stop_loss_buy):
    return False if side == 'buy' and symbol in ignore_stop_loss_buy else True


def place_market_order(symbol, side, quantity, b_client, client):
    # to_log(symbol, 'place_market_order Symbol {}, side {}, quantity {}'.format(symbol, side, quantity))
    if quantity is None:
        to_general_log(symbol, 'ERROR Qty is NONE', client)
        return
    if side == 'buy':
        order = b_client.order_market_buy(symbol=symbol, quantity=quantity, recvWindow=10000000)
        to_general_log(symbol, 'Order {} > {}'.format(side, json.dumps(order)), client)
        return order
    else:
        order = b_client.order_market_sell(symbol=symbol, quantity=quantity, recvWindow=10000000)
        to_general_log(symbol, 'Order {} > {}'.format(side, json.dumps(order)), client)
        return order


def get_opposite_side(side):
    return 'sell' if side == 'buy' else 'buy'


def place_pending_order(symbol, signal_side, price, minimal_quantity, stop_loss,
                        take_profits, preferred_wallet, precision, ignore_stop_loss_buy, b_client, client):
    try:
        # base = split_symbol(symbol)['base']
        # quote = split_symbol(symbol)['quote']
        # to_log(symbol, 'Place pending order ' + base + quote)
        global signal_number
        signal_id = '{}_{}'.format(symbol.lower(), signal_number)

        while True:
            if is_cancel(symbol, signal_side, client):
                # to_log(symbol, 'Cancel order')
                break

            # entrance point monitoring
            if is_entrance(symbol, signal_side, price, client):
                quantity = get_quantity(symbol, minimal_quantity, signal_side, preferred_wallet, client)
                if quantity is None:
                    return
                open_order = place_market_order(symbol, signal_side, quantity, b_client, client)
                signal_number += 1
                save_trade('{}_SIGNAL'.format(signal_side.upper()), signal_id, open_order, 'Entry', client)
                # comment = 'Order №{} Entry'.format(signal_number)
                # statement(symbol, open_order, comment, b_client)
                cumulativeQuoteQty = float(open_order['cummulativeQuoteQty'])
                flag = [False]
                order_side = get_opposite_side(signal_side)

                while True:

                    # stop loss monitoring
                    if is_stop_loss(symbol, signal_side, stop_loss, client):
                        if execute_stop_loss(symbol, signal_side, ignore_stop_loss_buy):
                            qty = get_qty(symbol, 'SL' if flag[0] is False else 'TP2',
                                          signal_side, take_profits, quantity, cumulativeQuoteQty,
                                          precision, client)
                            to_general_log(symbol, 'get_qty: qty {}'.format(qty), client)
                            place_market_order(symbol, order_side, qty, b_client, client)
                            save_trade('{}_SIGNAL'.format(signal_side.upper()), signal_id, open_order, 'SL', client)
                            client.set(symbol + '_last_order',
                                       json.dumps([order_side.upper(), price]))
                        else:
                            # to_log(symbol, 'Skip stop loss for buy signal')
                            to_general_log(symbol, 'Skip stop loss for buy signal', client)
                        break

                    # take profit targets monitoring
                    if is_take_profit(symbol, signal_side, take_profits, 0, client):
                        if flag[0] is False:
                            qty = get_qty(symbol, 'TP1', signal_side, take_profits, quantity, cumulativeQuoteQty,
                                          precision, client)
                            place_market_order(symbol, order_side, qty, b_client, client)
                            save_trade('{}_SIGNAL'.format(signal_side.upper()), signal_id, open_order, 'TP1', client)
                            stop_loss = price
                            flag[0] = True
                        else:
                            if is_take_profit(symbol, signal_side, take_profits, 1, client):
                                qty = get_qty(symbol, 'TP2', signal_side, take_profits, quantity, cumulativeQuoteQty,
                                              precision, client)
                                close_order = place_market_order(symbol, order_side, qty, b_client, client)
                                save_trade('{}_SIGNAL'.format(signal_side.upper()), signal_id, open_order, 'TP2', client)
                                client.set(symbol + '_last_order',
                                           json.dumps([order_side.upper(), close_order['price']]))
                                break
                break
            time.sleep(.1)
    except:
        to_general_log(symbol, traceback.format_exc(), client)


# place_pending_order('BNBETH', 'sell', 0.066210, 3, 0.066400,
#                     [{'target': 1, 'price': 0.065900, 'amount_percent': 50},
#                      {'target': 2, 'price': 0.065700, 'amount_percent': 100}])

# print(type(get_current_price('BNBETH', 'bid')), get_current_price('BNBETH', 'bid'))

# price = get_current_price('BNBETH', 'ask')
# print(price)
# qty = round_dwn(0.19637 / get_current_price('BNBETH', 'ask'), 2)
# print(qty)

# print(b_client.order_market_buy(symbol='BNBBTC', quantity=3))
# print(b_client.order_market_sell(symbol='BNBBTC', quantity=2.9))

# from binance.client import Client as bClient
#
# preferences = get_preferences()
# b_client = bClient(preferences['api_key'], preferences['secret_key'])
# print(b_client.get_asset_balance('BNB,')['free'])
# print(b_client.get_asset_balance('OMG')['free'])
# print(place_market_order('BNBBTC', 'buy', 3, b_client))


# print(b_client.get_asset_balance('BNB'))
# print(b_client.get_asset_balance('BTC'))


# print(b_client.get_asset_balance('ETH'))


# balances = b_client.get_account(recvWindow=10000000)['balances']
# for balance in balances:
#     if float(balance['free']) > 0:
#         to_log(symbol,'BNBETH', balance['asset'] + ' ' + balance['free'])
