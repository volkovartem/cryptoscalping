from database import get_total_profit, get_total_fees
from pymemcache.client.base import Client

print('PROFIT IN USDT', get_total_profit('USDT', Client(('127.0.0.1', 11211))))
print('FEES IN USDT', get_total_fees('BNB', 'USDT', Client(('127.0.0.1', 11211))))
