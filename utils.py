from datetime import datetime
import json
import time
from decimal import Decimal

import math

from models import Trade


def to_log(symbol, text):
    log_file_path = 'log/log.' + symbol + '.txt'
    # log_file_path = '/home/ubuntu/cryptoscalping/log/log.'  + symbol + '.txt'
    f = open(log_file_path, "a")
    dt = datetime.now().replace(microsecond=0).isoformat().replace('T', ' ')
    f.write(dt + ' ' + text + '\n')
    print(dt + ' ' + text)


def to_general_log(symbol, text, client):
    log = client.get('general_log')
    if log is None:
        log = '[]'
    log = json.loads(log)
    if len(log) > get_preferences()['max_log_length']:
        log = log[-(len(log) - 1):]

    dt = datetime.now().replace(microsecond=0).isoformat().replace('T', ' ')
    log.append(dt + ' ' + symbol + ': ' + text)
    client.set('general_log', json.dumps(log))
    print(dt + ' ' + symbol + ': ' + text)


def get_preferences():
    file_path = 'preferences.txt'
    # file_path = '/home/ubuntu/cryptoscalping/preferences.txt'
    ff = open(file_path, "r")
    preferences = json.loads(ff.read())
    ff.close()
    return preferences


def get_24_volume(symbol, client):
    tickers = client.get('tickers')
    if tickers is None:
        return
    tickers = json.loads(tickers)
    for ticker in tickers:
        if ticker['symbol'] == symbol:
            if (symbol == 'BNBBTC' or
                    symbol == 'BNBETH' or
                    symbol == 'BNBUSDT' or
                    symbol == 'BNBTUSD'):
                return float(ticker['volume'])
            return float(ticker['quoteVolume'])


def is_allowed(symbol, volume24_threshold_bnb, client):
    volume = get_24_volume(symbol, client)
    if volume is not None:
        if volume > volume24_threshold_bnb:
            return True
    return False


def get_pip(symbol, client):
    exchange_info = client.get('exchange_info')
    if exchange_info is None:
        return
    exchange_info = json.loads(exchange_info)
    symbols = exchange_info['symbols']
    for symbol_info in symbols:
        if symbol_info['symbol'] == symbol:
            return float(symbol_info['filters'][0]['tickSize'])


def split_symbol(symbol, client):
    exchange_info = client.get('exchange_info')
    if exchange_info is not None:
        symbols_info = json.loads(exchange_info)['symbols']
        for symbol_info in symbols_info:
            if symbol_info['symbol'] == symbol:
                return {'base': symbol_info['baseAsset'], 'quote': symbol_info['quoteAsset']}


def round_dwn(value, param=1):
    if param == 0:
        return int(value)
    return math.floor(value * math.pow(10, param)) / math.pow(10, param)


# def statement(symbol, data, comment, b_client): tm = datetime.utcfromtimestamp(int(data['transactTime'] /
# 1000)).strftime('%Y-%m-%d %H:%M:%S') s = '          ' line = 'Open order {} {}\n{} Time: {}\n{} ID: {}\n{}
# Quantity: {} {}\n{} Price: {} fee: {} {}\n{} Status: {}\n{} ' \ 'Comment: {}'.format(data['side'], data['symbol'],
# s, tm, s, data['orderId'], s, data['executedQty'], data['fills'][0]['commissionAsset'], s, data['fills'][0][
# 'price'], data['fills'][0]['commission'], data['fills'][0]['commissionAsset'], s, data['status'], s,
# comment) to_log(symbol, line) update_balances(b_client)


def get_balance(asset, b_client):
    return float(b_client.get_asset_balance(asset)['free'])


def get_balance2(asset, client):
    balances = client.get('BinanceBalance')
    if balances is not None:
        balances = json.loads(balances)
        print(balances)
        for balance in balances:
            if balance['asset'] == asset:
                return float(balance['free'])


def get_current_price(symbol, s, client):
    price = client.get(symbol)
    if price is None:
        return
    return float(json.loads(price)[s])


def get_min_notional(symbol, client):
    exchange_info = client.get('exchange_info')
    if exchange_info is None:
        return
    exchange_info = json.loads(exchange_info)
    symbols = exchange_info['symbols']
    for s in symbols:
        if s['symbol'] == symbol:
            for f in s['filters']:
                if f['filterType'] == 'MIN_NOTIONAL':
                    return float(f['minNotional'])


def get_precision(symbol, client):
    exchange_info = client.get('exchange_info')
    if exchange_info is None:
        return
    exchange_info = json.loads(exchange_info)
    symbols = exchange_info['symbols']
    for s in symbols:
        if s['symbol'] == symbol:
            for f in s['filters']:
                if f['filterType'] == 'LOT_SIZE':
                    step_size = Decimal(f['stepSize']).normalize()
                    return len(str(step_size).split('.')[1]) if '.' in str(step_size) else 0


def get_current_candle(symbol, time_frame, client):
    return json.loads(client.get(symbol + time_frame))[-2]


def get_candles(symbol, time_frame, client):
    return json.loads(client.get(symbol + time_frame))


def get_ma_value(symbol, time_frame, period, client):
    return json.loads(client.get(symbol + time_frame + 'MA' + period))[-1]


def check_quantity(symbol, min_quantity, balance, min_notional, quantity_quote, asset, quantity_base, base, client):
    if min_quantity > balance:
        to_general_log(symbol, 'Not enough balance. {} {} Required {} {}'.format(balance, asset, min_quantity, asset),
                       client)
        return False

    if quantity_quote < min_notional:
        to_general_log(symbol, 'Quantity is too low. {} {} Rise minimal BNB quantity'.format(quantity_base, base),
                       client)
        return False

    return True


def get_qty(symbol, order_type, side, take_profits, quantity, cumulative_quote_qty, precision, client):
    to_general_log(symbol, 'get_qty: {} {} take_profits {} quantity {} precision {}'
                   .format(order_type, side, take_profits, quantity, precision), client)
    if side == 'sell' and get_symbol_type(symbol, client):
        ask_price = get_current_price(symbol, 'ask', client)
        if ask_price is None:
            return
        quantity = cumulative_quote_qty / ask_price

    if order_type == 'SL':
        return round_dwn(quantity, precision)
    if order_type == 'TP1':
        return round_dwn(take_profits[0]['amount_percent'] / 100 * quantity, precision)
    if order_type == 'TP2':
        return round_dwn(take_profits[1]['amount_percent'] / 100 * quantity, precision)


def get_symbol_type(symbol, client):
    preferences = get_preferences()
    preferred_wallet = preferences['preferred_wallet']
    return split_symbol(symbol, client)['base'] == preferred_wallet


def get_quantity(symbol, min_bnb_quantity, side, preferred_wallet, client):
    base = split_symbol(symbol, client)['base']
    quote = split_symbol(symbol, client)['quote']
    minNotional = get_min_notional(symbol, client)
    precision = get_precision(symbol, client)

    if side == 'buy':
        ask_price = get_current_price(symbol, 'ask', client)
        balanceQuote = get_balance2(quote, client)
        print('BUY balanceQuote', balanceQuote)

        if quote == preferred_wallet:
            minQuantity = round(min_bnb_quantity, 8)
            quantityQuote = minQuantity
        else:
            minQuantity = round(min_bnb_quantity * ask_price, 8)
            quantityQuote = balanceQuote

        quantityBase = round_dwn(quantityQuote / ask_price, precision)

        if check_quantity(symbol, minQuantity, balanceQuote, minNotional,
                          quantityQuote, quote, quantityBase, base, client):
            print('quantityBase', quantityBase)
            return quantityBase

    if side == 'sell':
        bid_price = get_current_price(symbol, 'bid', client)
        balanceBase = get_balance2(base, client)
        print('SELL balanceBase', balanceBase)

        if quote == preferred_wallet:
            minQuantity = round(min_bnb_quantity / bid_price, 8)
            quantityBase = round_dwn(balanceBase, precision)
        else:
            minQuantity = round(min_bnb_quantity, 8)
            quantityBase = round_dwn(minQuantity, precision)

        quantityQuote = quantityBase * bid_price

        if check_quantity(symbol, minQuantity, balanceBase, minNotional,
                          quantityQuote, base, quantityBase, base, client):
            print('quantityBase', quantityBase)
            return quantityBase


def save_trade(signal_type, signal_id, order, trade_type, client):
    order_date = datetime.utcfromtimestamp(order['transactTime'] / 1000).strftime('%Y-%m-%d')
    order_time = datetime.utcfromtimestamp(order['transactTime'] / 1000).strftime('%H:%M:%S')

    for fill in order['fills']:
        trade = Trade(
            order_date=order_date,
            order_time=order_time,
            signal_type=signal_type,
            signal_id=signal_id,
            symbol=order['symbol'],
            side=order['side'],
            price=float(fill['price']),
            quantity=float(fill['qty']),
            quantity_asset=split_symbol(order['symbol'], client)['base'],
            fee=float(fill['commission']),
            fee_asset=fill['commissionAsset'],
            order_id=order['orderId'],
            status=order['status'],
            type=trade_type,
            order_timestamp=order['transactTime'],
            date_create=int(time.time())
        )
        trade.save()

# from pymemcache.client.base import Client
# save_trade('BUY_SIGNAL', 'bnbbtc_1', {
#     'symbol': 'NEOBNB',
#     'orderId': 27068094,
#     'clientOrderId': 'KwhRYReVu4Y6iyupIzfXUE',
#     'transactTime': 1557791833542,
#     'price': '0.00000000',
#     'origQty': '3.00000000',
#     'executedQty': '3.00000000',
#     'cummulativeQuoteQty': '1.24800000',
#     'status': 'FILLED',
#     'timeInForce': 'GTC',
#     'type': 'MARKET',
#     'side': 'BUY',
#     'fills': [
#         {
#             'price': '0.41600000',
#             'qty': '3.00000000',
#             'commission': '0.00093006',
#             'commissionAsset': 'BNB',
#             'tradeId': 1918158
#         }
#     ]
# }, 'Entry', Client(('127.0.0.1', 11211)))


# print(get_ma_value('BNBBTC', '5M', '8'))
# print(b_client.get_asset_balance('BNB', recvWindow=10000000))
# balanceBNB = float(b_client.get_asset_balance('ETH')['free'])
# print(balanceBNB)

# print(split_symbol('BNBBTC'))
# print(split_symbol('BNBUSDT'))
# print(split_symbol('BTCUSDT'))


# print(get_24_volume('BNBBTC'))
# print(get_24_volume('BNBETH'))
# print(get_24_volume('BNBUSDT'))
# print(get_24_volume('BNBTUSD'))
# print(get_24_volume('LTCBNB'))


# from binance.client import Client as bClient
# from pymemcache.client.base import Client
# preferences = get_preferences()
# b_client = bClient(preferences['api_key'], preferences['secret_key'])

# get_quantity('BNBBTC', 2, 'buy', 'BNB', Client(('127.0.0.1', 11211)), b_client)
# get_quantity('BNBBTC', 2, 'sell', 'BNB', Client(('127.0.0.1', 11211)), b_client)

# get_quantity('NEOBNB', 2, 'buy', 'BNB', Client(('127.0.0.1', 11211)), b_client)
# get_quantity('NEOBNB', 2, 'sell', 'BNB', Client(('127.0.0.1', 11211)), b_client)
# print(get_balance2('BNB', Client(('127.0.0.1', 11211))))
# print(get_balance2('BNB', Client(('127.0.0.1', 11211))))
#
# minimum = get_min_notional('BNBETH', b_client)
# print('minimum', type(minimum), minimum)
# minimum = get_precision('BNBETH', b_client)
# print('prcision', type(minimum), minimum)
#
#
# print(b_client.get_exchange_info())
#
# print(b_client.order_market_buy(symbol='BNBBTC', quantity=2, recvWindow=10000000))
#
# print(get_balance('NEO', b_client))
# print(get_balance('BNB', b_client))
# print(get_balance2(split_symbol('BNBPAX')['base'], Client(('127.0.0.1', 11211))))
# print(split_symbol('BNBPAX'))
