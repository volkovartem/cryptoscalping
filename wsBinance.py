import datetime
import json
import time
import traceback

import pandas as pd
import websocket
from binance.client import Client as bClient
from pymemcache.client.base import Client

from utils import to_general_log
from utils import get_preferences

client = Client(('127.0.0.1', 11211))
bclient = bClient(get_preferences()['api_key'], get_preferences()['secret_key'])


def calc_ma(klines, window, type_ma='Close'):
    data = pd.DataFrame(klines, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                         'Quote asset volume', 'Number of trades',
                                         'Taker buy base asset volume',
                                         'Taker buy quote asset volume', 'Ignore'])
    df = pd.DataFrame(data[type_ma])
    ma = df.rolling(window=window).mean()
    return list(ma.Close)


def mem_cache_init(symbol):
    time_now = datetime.datetime.utcfromtimestamp(time.time()).strftime('%H:%M:%S')
    history5m = bclient.get_klines(symbol=symbol, interval=bClient.KLINE_INTERVAL_5MINUTE)
    history1h = bclient.get_klines(symbol=symbol, interval=bClient.KLINE_INTERVAL_1HOUR)

    # for 5M
    time_frame = '5m'
    print(symbol, time_now, time_frame.upper(), history5m)
    client.set(symbol + time_frame.upper(), json.dumps(history5m))
    client.set(symbol + time_frame.upper() + 'MA8', json.dumps(calc_ma(history5m, 8)))
    client.set(symbol + time_frame.upper() + 'MA21', json.dumps(calc_ma(history5m, 21)))

    # for 1H
    time_frame = '1h'
    print(symbol, time_now, time_frame.upper(), history1h)
    client.set(symbol + time_frame.upper(), json.dumps(history1h))
    client.set(symbol + time_frame.upper() + 'MA8', json.dumps(calc_ma(history1h, 8)))
    client.set(symbol + time_frame.upper() + 'MA21', json.dumps(calc_ma(history1h, 21)))

    time.sleep(0.2)


def update_history(symbol, time_frame, kline):
    try:
        time_now = datetime.datetime.utcfromtimestamp(time.time()).strftime('%H:%M:%S')
        history = client.get(symbol + time_frame.upper())
        if history is not None:
            history = json.loads(history)
            history.pop(0)

            kline = [kline['t'], kline['o'], kline['h'],
                     kline['l'], kline['c'], kline['v'],
                     kline['T'], kline['q'], kline['n'],
                     kline['V'], kline['Q'], kline['B']]

            history.append(kline)

            print(symbol, time_now, time_frame.upper(), history)
            client.set(symbol + time_frame.upper(), json.dumps(history))
            client.set(symbol + time_frame.upper() + 'MA8', json.dumps(calc_ma(history, 8)))
            client.set(symbol + time_frame.upper() + 'MA21', json.dumps(calc_ma(history, 21)))
    except:
        to_general_log(symbol, 'Connect timeout. Reconnection...', client)


def on_message(ws, message):
    try:
        message = json.loads(message)
        stream = message['stream']
        data = message['data']

        if '@kline' in stream:
            symbol = data['s']
            kline = data['k']
            if bool(kline['x']):
                update_history(symbol, kline['i'], kline)

        if '@depth5' in stream:
            symbol = stream.split('@')[0].upper()
            client.set(symbol, json.dumps({'bid': data['bids'][0][0], 'ask': data['asks'][0][0]}))

    except KeyboardInterrupt:
        pass
    except:
        print(traceback.format_exc())


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print('### opened ###')


def launch():
    while True:
        print('start websocket Binance')
        time.sleep(1)

        symbols = get_preferences()['symbols']

        try:
            subs = ''
            for symbol in symbols:
                subs += symbol.lower() + '@kline_1h/'
                subs += symbol.lower() + '@kline_5m/'
                subs += symbol.lower() + '@depth5/'
                mem_cache_init(symbol)
            if subs != '':
                ws = websocket.WebSocketApp(
                    "wss://stream.binance.com:9443/stream?streams={}".format(subs.strip('/')),
                    on_open=on_open,
                    on_message=on_message,
                    on_error=on_error,
                    on_close=on_close)
                ws.run_forever()
        except:
            print('wsBinance Error: websocket failed')
            print(traceback.format_exc())


if __name__ == '__main__':
    try:
        launch()
    except KeyboardInterrupt:
        exit()
